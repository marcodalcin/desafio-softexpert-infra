# Desafio SoftExpert

Esse projeto contem os artefatos necessários para montar estrutura cloud no serviço EKS da Amazon:

-  Terraform para preparação da estrutura;
-  Gitlab, repositório de código;
-  Gitlab, no processo de CI/CD
-  Traefik, como proxy de acesso.

## Pré-Requisitos
-  Instalar e configurar o AWS-CLI (https://docs.aws.amazon.com/pt_br/streams/latest/dev/kinesis-tutorial-cli-installation.html)


## Como reproduzir:

1.  Faça um Fork do projeto;
2.  Crie as variáveis de ambiente, no Gitlab:

Supondo que já possua o AWS-CLI instalado
-  AWS_ACCESS_KEY_ID (ID gerado pelo aws config)
-  AWS_SECRET_ACCESS_KEY (KEY gerado pelo aws config)
-  AWS_DEFAULT_REGION (Zona/Região da sua conta AWS)

3. Faça commit no repositorio ou execute o pipeline no branch master;
> O procedimento de aplicar o Terraform, por questões estratégicas, está manual. Se for de seu interesse altere. Observe o pipeline para saber se o procedimento foi finalizado com sucesso! 
4. Seu cluster será criado de acordo especificações no Terraform;
5. Abra o terminal na pasta do projeto e exceute o código abaixo para instalar o Traefik:

`kubectl apply -f service.yml`

6. Be Happy!